import socket
import json
import threading
import time

PORT = 5050
SERVER = socket.gethostbyname(socket.gethostname())
ADDR = (SERVER, PORT)
DISCONNECT_MESSAGE = "!DISCONNECT"
CHECKAV_MESSAGE = "!CHECKAV"
AVAILABILITY = "INACTIVE"

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)

# function to set the status of the servers availability
def setAvailability(status):
    global AVAILABILITY
    AVAILABILITY = status
    print(f"[AVAILABILITY] Server is {AVAILABILITY}")

# function to check servers availabilty
def getAvailability():
    return AVAILABILITY

# fibonacci job function
def fibonacci(num):
    if num <= 1:
        return num
    return fibonacci(num-1) + fibonacci(num-2)

# bubblesort job function
def bubbleSort(arr):
    arrLen = len(arr)
    for i in range(arrLen-1, 0, -1):
        for j in range(i):
            if arr[j] > arr[j+1]:
                temp = arr[j]
                arr[j] = arr[j+1]
                arr[j+1] = temp

# function to request from master node
def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    while True:
        data = conn.recv(4096)
        decodedData = data.decode()
        if decodedData == DISCONNECT_MESSAGE:
            break

        elif decodedData == CHECKAV_MESSAGE:
            conn.send(getAvailability().encode())

        else:
            loadedData = json.loads(decodedData)
            if loadedData.get("req") == "!FIB":
                setAvailability("BUSY")
                num = loadedData.get("num")
                print("[JOB STATUS] Running...")
                fibNum = fibonacci(num)
                result = json.dumps({"fibNum": fibNum})
                print("[JOB STATUS] Finished")
                conn.send(result.encode())
                setAvailability("RUNNING")

            else:
                setAvailability("BUSY")
                data = json.loads(decodedData)
                arrToSort = data.get("arr")
                print("[JOB STATUS] Running...")
                bubbleSort(arrToSort)

                result = json.dumps({"sortedArray": arrToSort})
                print("[JOB STATUS] Finished")
                conn.send(result.encode())
                setAvailability("RUNNING")

    conn.close()
    setAvailability("RUNNING")

# start function
def start():
    server.listen()
    setAvailability("RUNNING")
    print(f"[LISTENING] Server is listening on {SERVER}")
    while True:
        conn, addr = server.accept()
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        print(f"[ACTIVE CONNECTION] {threading.activeCount() - 1}")


print(f"[STARTING] Server {SERVER} is starting...")
start()
