import socket
import json
import time

# master and worker node properties, state and messages ###
PORT = 5050
SERVER = ["18.141.181.233", "13.212.56.31"]
DISCONNECT_MESSAGE = "!DISCONNECT"
CHECKAV_MESSAGE = "!CHECKAV"
WORKER_USED = ""

# function for requesting sort function
def reqSort(arr):
    data = json.dumps({"req": "!SORT", "arr": arr})
    start = time.time()
    client.send(data.encode())
    print(f"\n[SENT] Job sent to Worker [{WORKER_USED}]")
    print("[JOB STATUS] Running...")

    data = client.recv(4096)
    end = time.time()
    print("[JOB STATUS] Finished")
    print(f"\n[RECEIVE] Result received from Worker [{WORKER_USED}]")

    result = json.loads(data.decode())
    print("============================")
    print(f'Sorted List: {result.get("sortedArray")}')
    print("============================")
    print(f'Excecution Time: {end-start}s')

# function for requesting fibonacci function
def reqFib(num):
    data = json.dumps({"req": "!FIB", "num": num})
    start = time.time()
    client.send(data.encode())
    print(f"\n[SENT] Job sent to Worker [{WORKER_USED}]")
    print("\n[JOB STATUS] Running...")

    data = client.recv(4096)
    end = time.time()
    print("[JOB STATUS] Finished")
    print(f"\n[RECEIVE] Result received from Worker [{WORKER_USED}]")

    result = json.loads(data.decode())

    print("============================")
    print(f'Fibonacci of {num} is: {result.get("fibNum")}')
    print("============================")

    print(f'Excecution Time: {end-start}s')

# function for checking worker status
def checkAv():
    for worker in SERVER:
        ADDR = (worker, PORT)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            client.connect(ADDR)

            avMsg = CHECKAV_MESSAGE.encode()
            client.send(avMsg)

            data = client.recv(4096)
            status = data.decode()

            dcMsg = DISCONNECT_MESSAGE.encode()
            client.send(dcMsg)
            print(f'[{worker}] Status: {status}')
        except:
            print(f'[{worker}] Status: INACTIVE')


def disconnect():
    dcMsg = DISCONNECT_MESSAGE.encode()
    client.send(dcMsg)
    client.close()

# function to select or change worker node
def chooseWorker():
    global client, WORKER_USED
    print("\nSelect Worker:")
    workerInput = input(
        f"(1) - Worker 1 [{SERVER[0]}]\n(2) - Worker 2 [{SERVER[1]}]\nEnter Worker Number: ")

    if workerInput == "1":
        ADDR = (SERVER[0], PORT)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(ADDR)
        WORKER_USED = SERVER[0]

    elif workerInput == "2":
        ADDR = (SERVER[1], PORT)
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect(ADDR)
        WORKER_USED = SERVER[1]

    else:
        print("Error: Invalid Worker")
        start()

# starter function for input
def start():
    active = True
    chooseWorker()
    while active:
        print("\n===== Main Menu =====")
        menuInput = input(
            "(1) - Sort a List \n(2) - Fibonnaci Number \n(3) - Show Worker Status \n(4) - Change Worker \n(5) - Exit \nEnter Menu Number: ")

        if menuInput == "1":
            print("\n===== Sort a List =====")
            userInput = input(
                "Enter list of numbers you want to sort (separate with space): \n")
            arrStr = userInput.split()
            try:
                arrInt = list(map(int, arrStr))
                reqSort(arrInt)
            except ValueError:
                print("Error: Please input list of numbers")

        elif menuInput == "2":
            print("\n===== Fibonacci Number ======")
            userInput = input("Fibonacci of: ")
            try:
                fibNum = int(userInput)
                reqFib(fibNum)
            except ValueError:
                print("Error: Not a number")

        elif menuInput == "3":
            print("\n===== Worker Status ======")
            checkAv()

        elif menuInput == "4":
            chooseWorker()

        elif menuInput == "5":
            print("Exit: Connection Closed")
            disconnect()
            active = False

        else:
            print(f'\nError: Invalid Menu')


start()
